" gcollura's [n]vimrc

filetype off

augroup vimrc
    autocmd!
augroup END

" Constants {{{
" get default ~/.config/nvim directory
let g:nvim_dir = stdpath('config')
let g:vim_dir = expand('~/.vim')
if has('nvim')
    let s:data_dir = stdpath('data')
else
    let s:data_dir = g:vim_dir
endif
let g:plugins_dir = s:data_dir . '/plugins'
let s:dein_dir = g:plugins_dir . '/repos/github.com/Shougo/dein.vim'
let s:swap_dir = g:vim_dir . '/swap'
let s:undo_dir = g:vim_dir . '/undo'
let g:ignore_pattern = '\(^\.\|node_modules\|^build$\|^[\.]*tags$\|\.pyc$\)'
" }}}

" Functions {{{
" Remove all the damned whitespaces
function! s:strip_whitespaces() abort
    " Save cursor position
    let pos = getpos(".")
    " Save last search
    let search = getreg('/')

    " Delete whitespaces
    %s/\s\+$//e

    " Restore everything
    call setpos('.', pos)
    call setreg('/', search)
endfunction

function! s:putdir(dir) abort
    let dir = expand(a:dir)
    if !isdirectory(dir)
        call mkdir(dir, 'p')
    endif
endfunction

function! s:edit_settings() abort
    if has('nvim')
        execute 'e' g:nvim_dir . '/init.vim'
    else
        let vimrc = g:vim_dir . '/vimrc'
        if filereadable(vimrc)
            execute 'e' vimrc
        else
            execute 'e' expand('~/.vimrc')
        endif
    endif
endfunction

" From http://got-ravings.blogspot.com/2008/07/vim-pr0n-visual-search-mappings.html
" makes * and # work on visual mode too. global function so user mappings can call it.
" specifying 'raw' for the second argument prevents escaping the result for vimgrep
" TODO: there's a bug with raw mode. since we're using @/ to return an unescaped
" search string, vim's search highlight will be wrong. Refactor plz.
function! s:visual_star_search_set(cmdtype,...) abort
    let temp = @"
    normal! gvy
    if !a:0 || a:1 != 'raw'
        let @" = escape(@", a:cmdtype.'\*')
    endif
    let @/ = substitute(@", '\n', '\\n', 'g')
    let @" = temp
endfunction

" Zoom / Restore window.
function! s:zoom_toggle() abort
    if exists('t:zoomed') && t:zoomed
        execute t:zoom_winrestcmd
        let t:zoomed = 0
    else
        let t:zoom_winrestcmd = winrestcmd()
        resize
        vertical resize
        let t:zoomed = 1
    endif
endfunction

function! s:find_file(dir, filename) abort
    let l:found = globpath(a:dir, filename)
    if filereadable(l:found)
        return l:found
    endif

    let l:parent = fnamemodify(a:dir, ':h')
    if l:parent != a:dir
        return s:find_file(l:parent, filename)
    endif

    return ''
endfunction

" copy to attached terminal using the yank(1) script:
" https://github.com/sunaku/home/blob/master/bin/yank
function! s:yank(text) abort
    let escape = system('yank', a:text)
    if v:shell_error
        echoerr escape
    else
        call writefile([escape], '/dev/tty', 'b')
    endif
endfunction

function! s:create_virtualenv(py_version, path, requirements_txt, ...) abort
    let a:force = get(a:, 1, 0)
    if isdirectory(a:path) && a:force
        execute '!' . 'rm -r' a:path
    endif
    if !isdirectory(a:path)
        execute '!' . 'virtualenv -p' a:py_version a:path
        execute '!' . 'source' a:path . '/bin/activate && pip install -U pip setuptools'
        execute '!' . 'source' a:path . '/bin/activate && pip install -r' a:requirements_txt
    endif
endfunction

function! s:update_virtualenv(path, requirements_txt) abort
    if isdirectory(a:path)
        execute '!' . 'source' a:path . '/bin/activate && pip install -Ur' a:requirements_txt
    endif
endfunction
" }}}

" nvim python config {{{
if has('nvim')
    let s:python2_dir = s:data_dir . '/python/pyenv2'
    let s:python3_dir = s:data_dir . '/python/pyenv3'
    let s:requirements_txt = g:nvim_dir . '/requirements.txt'

    call s:create_virtualenv('python2', s:python2_dir, s:requirements_txt)
    call s:create_virtualenv('python3', s:python3_dir, s:requirements_txt)

    let g:python_host_prog = s:python2_dir . '/bin/python'
    let g:python3_host_prog = s:python3_dir . '/bin/python'

    let $PATH .= ':' . s:python3_dir . '/bin'
endif
" }}}

" Commands {{{
command! Settings call s:edit_settings()
command! StripTrailingWhitespaces call s:strip_whitespaces()
command! ZoomToggle call s:zoom_toggle()
command! Yank call s:yank(@0)
command! -nargs=1 VisualStarSearchSet call s:visual_star_search_set(<f-args>)

command! BashRc :e ~/.bashrc
command! ZshRc :e ~/.zshrc
command! W :w
command! Q :q

if has('nvim')
    command! UpdateVirtualenv call s:update_virtualenv(s:python2_dir, s:requirements_txt)
    command! UpdateVirtualenv3 call s:update_virtualenv(s:python3_dir, s:requirements_txt)
endif
" }}}

" Encoding and fileformats {{{
set fileformat=unix " default
set fileformats=unix,mac,dos
" }}}

" Editing {{{
set undofile
set clipboard+=unnamedplus

set confirm
set hidden

set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set smarttab

set autowriteall

autocmd vimrc FocusLost * silent! wa

if !has('nvim')
    let &directory = s:swap_dir
    call s:putdir(s:swap_dir)
    let &undodir = s:undo_dir
    call s:putdir(s:undo_dir)
endif
" }}}

" Folding {{{
set foldmethod=indent
set foldenable
set foldlevelstart=1
" }}}

" Visual and display {{{
set breakindent
set showbreak=\ \
set nowrap

set lazyredraw
set noshowmode
set modeline

set shortmess+=c " Do not display completion messages

set diffopt+=vertical

set number
set relativenumber
set numberwidth=5

set scrolloff=4
set cursorline
set mouse=a

set textwidth=79
set colorcolumn=+1

set list
set listchars=tab:▸\ ,eol:¬

if exists('&inccommand')
    set inccommand=split
endif

if exists('&termguicolors')
    set termguicolors
endif

if exists('&guicursor')
    set guicursor=n-v-c:block-Cursor/lCursor-blinkon0,i-ci:ver25-Cursor/lCursor,r-cr:hor20-Cursor/lCursor
endif

" Vim italics escaping codes {{{
set t_ZH=[3m
set t_ZR=[23m
" }}}

" neovim terminal options {{{
" Use cursor shape feature
let $TMUX_TUI_ENABLE_SHELL_CURSOR = 1
" Use true color feature
let $NVIM_TUI_ENABLE_TRUE_COLOR = 1
" }}}
" }}}

" Spell {{{
set nospell
set spelllang=en_us,it
set spellsuggest=5
" }}}

" Search {{{
set hlsearch
set incsearch
set ignorecase
set smartcase
set gdefault
set showmatch

set regexpengine=1
" }}}

" Completion {{{
set wildmenu
set wildmode=longest,full
" stuff to ignore when tab completing
set wildignore=*.o,*.png,*.gif,*.jpg,*.jpeg,*.zip,*.jar,*.gem,coverage/**,log/**,*.pyc,*~,build/**
set tags=.tags,/
" }}}

" Mappings {{{
" Generic shortcuts
inoremap jj <Esc>
noremap Q <nop>

" Motion
nnoremap j gj

" Toggle search highlight
noremap <leader>h :nohlsearch<CR>
noremap <leader>w :w!<CR>

" delitMate autoclose and indent
" inoremap {<CR> {<CR>}<C-o>O

" This rewires n and N to do the highlighing...
" replace vim's built-in visual * and # behavior
xnoremap * :<C-u>VisualStarSearchSet '/'<CR>/<C-R>=@/<CR><CR>
xnoremap # :<C-u>VisualStarSearchSet '?'<CR>?<C-R>=@/<CR><CR>

nnoremap <silent> <leader>z :ZoomToggle<CR>
noremap <silent> <leader>y y:<C-U>Yank<CR>

augroup command_window
    autocmd!
    autocmd CmdwinEnter * nmap <buffer> <Esc>   :q<CR>
    autocmd CmdwinEnter * nmap <buffer> q       :q<CR>
augroup END

" terminal {{{
if exists(':terminal')
    tnoremap <ESC>      <C-\><C-n>
    tnoremap jj         <C-\><C-n>
    tnoremap j<Space>   j
    tnoremap <leader>q  <C-\><C-n>:<C-u>q!<CR>

    nnoremap <leader>t  :<C-u>below 10sp term://$SHELL<cr>i
    nnoremap <Leader>T  :<C-u>terminal<CR>
    nnoremap !          :<C-u>terminal<Space>

    if has('nvim')
        autocmd vimrc TermOpen * call s:init_terminal_settings()
    endif
    function! s:init_terminal_settings() abort
        nnoremap <buffer> <leader>q :<C-u>bdelete!<CR>
    endfunction
endif
" }}}
" }}}

" Plugins {{{
if !isdirectory(s:dein_dir)
    execute '!git clone https://github.com/Shougo/dein.vim' s:dein_dir
endif
if has('vim_starting')
  let &runtimepath .= ',' . s:dein_dir
endif

let g:dein#install_process_timeout = 240

" Plugins installation {{{
if dein#load_state(g:plugins_dir)
    call dein#begin(g:plugins_dir)

    " Dein {{{
    call dein#add(s:dein_dir)
    " }}}

    " Vimproc.vim {{{
    call dein#add('Shougo/vimproc.vim', {
                \ 'build': 'make'
                \ })
    " }}}

    " Denite.nvim {{{
    call dein#add('Shougo/denite.nvim', {
                \ 'on_cmd': 'Denite',
                \ 'hook_source': 'call DeniteOnSource()'
                \ })
    " }}}

    " Defx.nvim {{{
    call dein#add('Shougo/defx.nvim', {
                \ 'on_cmd': 'Defx'
                \ })
    if !has('nvim')
        call dein#add('roxma/nvim-yarp', {
                    \ 'on_source': 'defx.nvim'
                    \ })
        call dein#add('roxma/vim-hug-neovim-rpc', {
                    \ 'on_source': 'defx.nvim'
                    \ })
    endif
    " }}}

    " Denite.nvim plugins {{{
    call dein#add('Shougo/neomru.vim', {
                \ 'on_source': 'denite.nvim'
                \ })
    call dein#add('neoclide/denite-extra', {
                \ 'on_source': 'denite.nvim'
                \ })
    " }}}

    " You complete me {{{
    call dein#add('Valloric/YouCompleteMe', {
                \ 'merged': 0
                \ })
    " }}}

    " Neomake {{{
    call dein#add('neomake/neomake', {
                \ 'on_cmd': 'Neomake',
                \ 'hook_source': 'call NeomakeOnSource()'
                \ })
    " }}}

    " Vim statusline {{{
    call dein#add('vim-airline/vim-airline')
    call dein#add('vim-airline/vim-airline-themes')
    " }}}

    " choosewin {{{
    call dein#add('t9md/vim-choosewin', {
                \ 'on_map': '<Plug>(choosewin)',
                \ 'on_cmd': 'Defx'
                \ })
    " }}}

    " vim bbye delete current buffer {{{
    call dein#add('moll/vim-bbye', {
                \ 'on_cmd': 'Bdelete'
                \ })
    " }}}

    " Ultisnips and Snippets {{{
    call dein#add('SirVer/ultisnips')
    call dein#add('honza/vim-snippets')
    " }}}

    " Commentary {{{
    call dein#add('tomtom/tcomment_vim', {
                \ 'on_map': [ 'g' ]
                \ })
    " }}}

    " Vim fugitive {{{
    call dein#add('tpope/vim-fugitive')
    " }}}

    " Vim repeat {{{
    call dein#add('tpope/vim-repeat')
    " }}}

    " Vim eunuch {{{
    call dein#add('tpope/vim-eunuch', {
                \ 'on_cmd': [ 'Unlink', 'Remove', 'Move', 'Rename', 'Chmod',
                \   'Mkdir', 'Find', 'Locate', 'SudoEdit', 'SudoWrite', 'W'
                \ ]})
    " }}}

    " tmux focus events {{{
    call dein#add('tmux-plugins/vim-tmux-focus-events', {
                \ 'if': exists('$TMUX'),
                \ })
    " }}}

    " Operator and text objects {{{
    call dein#add('kana/vim-operator-user')
    call dein#add('kana/vim-textobj-user')

    call dein#add('rhysd/vim-operator-surround')
    call dein#add('sgur/vim-textobj-parameter')
    call dein#add('glts/vim-textobj-comment')
    " }}}

    " delimitMate {{{
    call dein#add('Raimondi/delimitMate', {
                \ 'on_i': 1
                \ })
    " }}}

    " Vim easy align {{{
    call dein#add('junegunn/vim-easy-align', {
                \ 'on_cmd': 'EasyAlign'
                \ })
    " }}}

    " incsearch.vim {{{
    call dein#add('haya14busa/incsearch.vim')
    call dein#add('haya14busa/incsearch-fuzzy.vim', {
                \ 'depends': 'incsearch.vim',
                \ 'on_map': '<Plug>(incsearch-'
                \ })
    " }}}

    " vim-searchant{{{
    call dein#add('timakro/vim-searchant')
    " }}}

    " Undotree {{{
    call dein#add('mbbill/undotree', {
                \ 'on_cmd': 'UndotreeToggle'
                \ })
    " }}}

    " Gnupg support -- not working on neovim {{{
    call dein#add('jamessan/vim-gnupg', {
                \ 'augroup': 'GnuPG',
                \ 'on_path': [ '\.gpg$', '\.pgp$', '\.asc$' ],
                \ 'if': 0
                \ })
    " }}}

    " Latex {{{
    call dein#add('lervag/vimtex')
    " }}}

    " Pad {{{
    call dein#add('fmoralesc/vim-pad', {
                \ 'on_cmd': 'Pad'
                \ })
    " }}}

    " file-line {{{
    " Plugin for vim to enable opening a file in a given line
    " :e file.ext:line
    call dein#add('bogado/file-line')
    " }}}

    " clang-format {{{
    call dein#add('rhysd/vim-clang-format', {
                \ 'on_map': '<Plug>(operator-clang-format)',
                \ 'on_cmd': 'ClangFormat'
                \ })
    " }}}

    " Filetype plugins {{{
    " C++11 support {{{
    call dein#add('octol/vim-cpp-enhanced-highlight', {
                \ 'on_ft': [ 'c', 'cpp' ]
                \ })
    " }}}

    " Java 8 support {{{
    call dein#add('vim-jp/vim-java', {
                \ 'on_ft': 'java'
                \ })
    " }}}

    " Rust support {{{
    call dein#add('rust-lang/rust.vim', {
                \ 'on_ft': 'rust'
                \ })
    " }}}

    " Scss support {{{
    call dein#add('cakebaker/scss-syntax.vim', {
                \ 'on_ft': [ 'scss', 'sass', 'css' ]
                \ })
    " }}}

    " Pug support {{{
    call dein#add('digitaltoad/vim-pug', {
                \ 'on_ft': [ 'jade', 'pug' ]
                \ })
    " }}}

    " Javascript and HTML support {{{
    call dein#add('gavocanov/vim-js-indent', {
                \ 'on_ft': [ 'javascript', 'html' ]
                \ })
    call dein#add('othree/yajs.vim', {
                \ 'on_ft': [ 'javascript', 'html' ]
                \ })
    call dein#add('othree/javascript-libraries-syntax.vim', {
                \ 'on_ft': [ 'javascript' ]
                \ })
    " }}}

    " matchtag for xml and html {{{
    call dein#add('gregsexton/MatchTag', {
                \ 'on_ft': [ 'html', 'xml' ]
                \ })
    " }}}

    " XML support {{{
    call dein#add('sukima/xmledit', {
                \ 'on_ft': [ 'html', 'xml', 'php' ]
                \ })
    " }}}

    " HTMLv5 support {{{
    call dein#add('othree/html5.vim', {
                \ 'on_ft': [ 'html' ]
                \ })
    " }}}

    " Vimlang omni complete support {{{
    call dein#add('c9s/vimomni.vim')
    " }}}

    " Tmux configuration support {{{
    call dein#add('tmux-plugins/vim-tmux', {
                \ 'on_ft': 'tmux',
                \ 'on_path': '.*tmux.conf'
                \ })
    " }}}

    " Toml support {{{
    call dein#add('cespare/vim-toml', {
                \ 'on_ft': 'toml'
                \ })
    " }}}

    " nginx configuration support {{{
    call dein#add('vim-scripts/nginx.vim', {
                \ 'on_ft': 'nginx'
                \ })
    " }}}

    " Apache Pig support {{{
    call dein#add('motus/pig.vim', {
                \ 'on_path': [ '\.pig$' ]
                \ })
    " }}}

    " Go Lang support {{{
    call dein#add('fatih/vim-go', {
                \ 'on_ft': 'go'
                \ })
    " }}}

    " Python support {{{
    call dein#add('Vimjas/vim-python-pep8-indent')
    call dein#add('jmcantrell/vim-virtualenv', {
                \ 'on_ft': 'python',
                \ 'hook_source': 'YcmRestartServer'
                \ })
    " }}}

    " Puppet support {{{
    call dein#add('rodjek/vim-puppet')
    " }}}
    " }}}

    " colorschemes {{{
    " base16 colorscheme {{{
    call dein#add('chriskempson/base16-vim')
    " }}}
    " vim one colorscheme {{{
    call dein#add('rakr/vim-one')
    " }}}
    " }}}

    call dein#end()
    call dein#save_state()
    call dein#check_install()
endif
" }}}

" Plugins configuration {{{

" denite.nvim {{{
if dein#tap('denite.nvim')
    function! DeniteOnSource() abort
        call denite#custom#option('default', {
                    \ 'prompt': '»',
                    \ 'winheight': 13,
                    \ 'ignorecase' : 1,
                    \ 'direction': 'dynamictop',
                    \ 'matchers': 'matcher/fuzzy'
                    \ })

        " Change source configs.
        call denite#custom#source('file/rec', 'sorters', ['sorter/sublime'])
        call denite#custom#source(
                    \ 'file_mru', 'matchers', ['matcher/fuzzy', 'matcher/project_files'])
        " file/rec/git
        call denite#custom#alias('source', 'file/rec/git', 'file/rec')
        call denite#custom#var('file/rec/git', 'command',
                    \ ['git', 'ls-files', '-co', '--exclude-standard'])

        if executable('rg')
            call denite#custom#var('file/rec', 'command',
                        \ ['rg', '--files', '--glob', '--hidden', '!.git'])

            call denite#custom#var('grep', 'command', ['rg'])
            call denite#custom#var('grep', 'recursive_opts', [])
            call denite#custom#var('grep', 'final_opts', [])
            call denite#custom#var('grep', 'separator', ['--'])
            call denite#custom#var('grep', 'default_opts',
                        \ ['--vimgrep', '--no-heading'])
        elseif executable('ag')
            call denite#custom#var('file/rec', 'command',
                        \ ['ag', '--follow', '--nocolor', '--nogroup', '--hidden', '-g', ''])

            call denite#custom#var('grep', 'command', ['ag'])
            call denite#custom#var('grep', 'default_opts',
                        \ ['-i', '--vimgrep'])
            call denite#custom#var('grep', 'separator', ['--'])
            call denite#custom#var('grep', 'recursive_opts', [])
            call denite#custom#var('grep', 'final_opts', [])
        endif

        call denite#custom#map(
                    \ 'insert',
                    \ '<Down>',
                    \ '<denite:move_to_next_line>',
                    \ 'noremap'
                    \ )
        call denite#custom#map(
                    \ 'insert',
                    \ '<Up>',
                    \ '<denite:move_to_previous_line>',
                    \ 'noremap'
                    \ )
        call denite#custom#map(
                    \ 'insert',
                    \ '<Esc>',
                    \ '<denite:enter_mode:normal>'
                    \ )
    endfunction
    nnoremap <leader>p :<C-u>Denite
                \ `finddir('.git', ';') != '' ? 'file/rec/git' : 'file/rec'` <CR>
    nnoremap <leader>[ :<C-u>Denite grep <CR>
    nnoremap <leader>l :<C-u>Denite -source-names="hide" -mode="normal" buffer:- <CR>
    nnoremap <leader>r :<C-u>Denite -mode="normal" register <CR>
    nnoremap <leader>L :<C-u>Denite line <CR>
    nnoremap <leader>R :<C-u>Denite -resume <CR>
    nnoremap <leader>s :<C-u>Denite outline <CR>
    nnoremap <leader>c :<C-u>Denite -mode="normal" quickfix location_list<CR>
    nnoremap <leader>o :<C-u>Denite -source-names="hide" file_mru file<CR>
endif
" }}}

" defx.nvim {{{
if dein#tap('defx.nvim')
    nnoremap <leader>f :<C-u>Defx
                \ -split=vertical -winwidth=40 -direction=topleft -toggle
                \ -buffer-name=defx <CR>
    autocmd vimrc FileType defx call s:defx_my_settings()
    autocmd vimrc FileType defx setl nonumber norelativenumber
    function! s:defx_my_settings() abort
        " Define mappings
        nnoremap <silent><buffer><expr> <CR>
                    \ defx#do_action('open', 'wincmd p \| drop')
        nnoremap <silent><buffer><expr> o
                    \ defx#do_action('open', 'wincmd p \| split')
        nnoremap <silent><buffer><expr> O
                    \ defx#do_action('open', 'wincmd p \| vsplit')
        nnoremap <silent><buffer><expr> <space>
                    \ defx#do_action('toggle_select') . 'j'
        nnoremap <silent><buffer><expr> q
                    \ defx#do_action('quit')
        nnoremap <silent><buffer><expr> l
                    \ defx#do_action('open', 'wincmd p \| drop')
        nnoremap <silent><buffer><expr> h
                    \ defx#do_action('cd', ['..'])
        nnoremap <silent><buffer><expr> j
                    \ line('.') == line('$') ? 'gg' : 'j'
        nnoremap <silent><buffer><expr> k
                    \ line('.') == 1 ? 'G' : 'k'
        nnoremap <silent><buffer><expr> <bs>
                    \ defx#do_action('cd', ['..'])
        nnoremap <silent><buffer><expr> .
                    \ defx#do_action('toggle_ignored_files')
    endfunction
endif
" }}}

" youcompleteme {{{
if dein#tap('YouCompleteMe')
    let g:ycm_global_ycm_extra_conf = g:nvim_dir . '/ycm_extra_conf.py'
    let g:ycm_autoclose_preview_window_after_insertion = 1
    let g:ycm_complete_in_comments = 1
    let g:ycm_confirm_extra_conf = 0 " NOTE: Extremely insecure!
    let g:ycm_collect_identifiers_from_tags_files = 1
    let g:ycm_warning_symbol = '»'
    let g:ycm_error_symbol = '»'
    let g:ycm_always_populate_location_list = 1
    let g:ycm_rust_src_path = expand('~/.multirust/toolchains/stable-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/src')
    let g:ycm_goto_buffer_command = 'horizontal-split'
    let g:ycm_python_binary_path = 'python'
    nnoremap <leader>d :YcmCompleter GoToDefinitionElseDeclaration<CR>
endif
" }}}

" neomake {{{
if dein#tap('neomake')
    let g:neomake_warning_sign = {
                \ 'text': '»',
                \ 'texthl': 'WarningMsg',
                \ }
    let g:neomake_error_sign = {
                \ 'text': '✗',
                \ 'texthl': 'ErrorMsg',
                \ }
    let g:neomake_info_sign = {
                \ 'text': '▸',
                \ 'texthl': 'NeomakeInfoSign',
                \ }
    let g:neomake_message_sign = {
                \ 'text': '▸',
                \ 'texthl': 'NeomakeMessageSign',
                \ }
    let g:neomake_python_enabled_makers = ['flake8', 'frosted', 'pylint', 'pycodestyle']
    let g:neomake_javascript_enabled_makers = ['jshint', 'jscs']
    let g:neomake_jsx_enabled_makers = ['jscs']
    let g:neomake_php_phpcs_args_standard = 'WordPress-Core'
    let g:neomake_c_clang_maker = {
                \ 'args': [ '-fsyntax-only', '-Wall', '-Wextra', '-lpthread' ],
                \ 'errorformat':
                \ '%-G%f:%s:,' .
                \ '%f:%l:%c: %trror: %m,' .
                \ '%f:%l:%c: %tarning: %m,' .
                \ '%f:%l:%c: %m,'.
                \ '%f:%l: %trror: %m,'.
                \ '%f:%l: %tarning: %m,'.
                \ '%f:%l: %m',
                \ }
    let g:neomake_rust_rustc_maker = {
                \ 'args': [ 'rustc', '-Zno-trans' ],
                \ 'exe': 'cargo',
                \ 'append_file': 0,
                \ 'errorformat':
                \ '%-G%f:%s:,' .
                \ '%f:%l:%c: %trror: %m,' .
                \ '%f:%l:%c: %tarning: %m,' .
                \ '%f:%l:%c: %m,'.
                \ '%f:%l: %trror: %m,'.
                \ '%f:%l: %tarning: %m,'.
                \ '%f:%l: %m',
                \ }
    function! NeomakeOnSource() abort
        " This will trigger Neomake on TextChanged, InsertLeave, BufWritePost and
        " BufWinEnter, with a delay of 500ms by default, but 0 for BufWritePost.
        call neomake#configure#automake({
                    \ 'TextChanged': {},
                    \ 'InsertLeave': {},
                    \ 'BufWritePost': {'delay': 0},
                    \ 'BufWinEnter': {},
                    \ }, 500)
    endfunction
endif
" }}}

" vim-airline {{{
if dein#tap('vim-airline')
    let g:airline_powerline_fonts = 0
    let g:airline_exclude_preview = 1
endif
" }}}

" vim-choosewin {{{
if dein#tap('vim-choosewin')
    let g:choosewin_overlay_enable = 1
    let g:choosewin_overlay_font_size = 'small'
    let g:choosewin_blink_on_land = 0
    let g:choosewin_return_on_single_win = 1
    let s:ignore_filetype = ['unite', 'vimfiler', 'denite', 'defx']
    let g:choosewin_hook = {}
    function! g:choosewin_hook.filter_window(winnums) abort
        return filter(a:winnums,
                    \ 'index(s:ignore_filtype,
                    \   getwinvar(v:val, "&filetype")) == -1')
    endfunction
    nmap - <Plug>(choosewin)
endif
" }}}

" vim-bbye {{{
if dein#tap('vim-bbye')
    nnoremap <leader>q :<C-u>Bdelete <CR>
endif
" }}}

" ultisnips {{{
if dein#tap('ultisnips')
    let g:UltiSnipsUsePythonVersion = 3
    let g:UltiSnipsListSnippets = '<c-l>'
    let g:UltiSnipsExpandTrigger = '<c-h>'
    let g:UltiSnipsJumpForwardTrigger = "<c-j>"
    let g:UltiSnipsJumpBackwardTrigger = "<c-k>"
endif
" }}}

" vim-operator-surround {{{
if dein#tap('vim-operator-surround')
    map <silent>sa <Plug>(operator-surround-append)
    map <silent>sd <Plug>(operator-surround-delete)
    map <silent>sr <Plug>(operator-surround-replace)
endif
" }}}

" incsearch.vim {{{
if dein#tap('incsearch.vim')
    let g:incsearch#auto_nohlsearch = 1
    map /  <Plug>(incsearch-forward)
    map ?  <Plug>(incsearch-backward)
    map g/ <Plug>(incsearch-stay)
    map z/ <Plug>(incsearch-fuzzy-/)
    map z? <Plug>(incsearch-fuzzy-?)
    map zg/ <Plug>(incsearch-fuzzy-stay)
endif
" }}}

" undotree {{{
if dein#tap('undotree')
    let g:undotree_SetFocusWhenToggle = 1
    nnoremap <leader>u :<C-u>UndotreeToggle <CR>
endif
" }}}

" vim-gnupg {{{
if dein#tap('vim-gnupg')
    let g:GPGPreferArmor = 1 " Tell the GnuPG plugin to armor new files.
    let g:GPGPreferSign = 1 " Tell the GnuPG plugin to sign new files.
    autocmd vimrc BufReadCmd,FileReadCmd *.gpg,*.asc,*.pgp call dein#source('vim-gnupg')
endif
" }}}

" vimtex {{{
if dein#tap('vimtex')
    let g:vimtex_compiler_latexmk = {
                \ 'backend' : 'nvim',
                \ 'background' : 1,
                \ 'build_dir' : 'out',
                \ 'callback' : 1,
                \ 'continuous' : 1,
                \ 'executable' : 'latexmk',
                \ 'options' : [
                \   '-pdf',
                \   '-verbose',
                \   '-file-line-error',
                \   '-shell-escape',
                \   '-synctex=1',
                \   '-interaction=nonstopmode',
                \ ],
                \}
    let g:vimtex_mappings_enabled = 1
    let g:vimtex_quickfix_mode = 1
    let g:vimtex_view_method = 'mupdf'
    let g:vimtex_quickfix_warnings = {
                \ 'overfull' : 0,
                \ 'underfull' : 0,
                \ 'packages' : {
                \   'default' : 0,
                \ },
                \}
    let g:vimtex_format_enabled = 1
    if !exists('g:ycm_semantic_triggers')
        let g:ycm_semantic_triggers = {}
    endif
    let g:ycm_semantic_triggers.tex = [
                \ 're!\\[A-Za-z]*cite[A-Za-z]*(\[[^]]*\]){0,2}{[^}]*',
                \ 're!\\[A-Za-z]*ref({[^}]*|range{([^,{}]*(}{)?))',
                \ 're!\\includegraphics\*?(\[[^]]*\]){0,2}{[^}]*',
                \ 're!\\(include(only)?|input){[^}]*'
                \ ]
endif
" }}}

" vim-pad {{{
if dein#tap('vim-pad')
    if isdirectory(expand('~/ownCloud/Notes'))
        let g:pad#dir = '~/ownCloud/Notes'
    else
        let g:pad#dir = '~/.notes/'
        call s:putdir(g:pad#dir)
    endif
endif
" }}}

" vim-clang-format {{{
if dein#tap('vim-clang-format')
    let g:clang_format#code_style = 'llvm'
    let g:clang_format#detect_style_file = 1
    let g:clang_format#style_options = {
                \ 'Standard': 'C++11',
                \ 'AllowShortIfStatementsOnASingleLine': 'true',
                \ 'AllowShortLoopsOnASingleLine': 'true',
                \ 'SpaceAfterCStyleCast': 'true'
                \ }
    autocmd vimrc FileType c,cpp,objc map <buffer><leader>x <Plug>(operator-clang-format)
endif
" }}}

" javascript-libraries-syntax.vim {{{
if dein#tap('javascript-libraries-syntax.vim')
    let g:used_javascript_libs = 'jquery,angularjs,angularui,angularuirouter,chai'
endif
" }}}

" vim-go {{{
if dein#tap('vim-go')
    let g:go_version_warning = 0
endif
" }}}

" vim-one {{{
if dein#tap('vim-one')
    let g:one_allow_italics = 1 " Not italics over ssh
endif
" }}}
" }}}
" }}}

" Eclimd configuration {{{
let g:EclimCompletionMethod = 'omnifunc'
let g:EclimJavascriptValidate = 0
let g:EclimFileTypeValidate = 0
let g:EclimJavaValidate = 1
" }}}

" Colors {{{
syntax enable
set background=dark

if $BASE16_CURRENT
    if !has('gui_running')
        set t_Co=256
        let base16colorspace = 256
    endif
    colorscheme base16-default-dark
    let g:airline_theme = 'base16'
else
    silent! colorscheme one
    let g:airline_theme = 'one'
endif
" }}}

" Filetypes {{{
augroup filetype_rust
    autocmd!
    autocmd BufNewFile,BufRead *.rs         set filetype=rust
    autocmd BufNewFile,BufRead *.toml       set filetype=toml
    autocmd BufNewFile,BufRead Cargo.lock   set filetype=toml
augroup END

augroup filetype_html
    autocmd!
    autocmd FileType html,xhtml,xml         setl tabstop=2 shiftwidth=2 matchpairs+=<:>
    autocmd FileType html                   setl omnifunc=htmlcomplete#CompleteTags noci si
    autocmd FileType html                   let g:html_indent_inctags = 'html,body,head,tbody'
    autocmd FileType xml,xslt               setl noexpandtab textwidth=0
    autocmd FileType xml                    setl omnifunc=xmlcomplete#CompleteTags noci
    autocmd FileType xml,xslt               let maplocalleader = "_"
augroup END

augroup filetype_tex
    autocmd!
    autocmd BufNewFile *.tex                set filetype=tex
    autocmd FileType tex                    setl wrap linebreak spell
    autocmd FileType tex                    let maplocalleader = "_"
    " autocmd FileType tex                    setl flp="^\s*\\(item\|end\|begin)*\s*"
    " fo=tcqjan
augroup END

augroup filetype_js
    autocmd!
    autocmd FileType javascript                 setl tabstop=2 shiftwidth=2
    autocmd FileType json                       setl tabstop=4 shiftwidth=4
    autocmd BufRead,BufNewFile .jshintrc        set filetype=json
    autocmd BufRead,BufNewFile .jscsrc          set filetype=json
    autocmd BufRead,BufNewFile .tern-project    set filetype=json
augroup END

augroup filetype_java
    autocmd!
    autocmd FileType java                   nnoremap <buffer> <C-O> :JavaImportOrganize <CR>
    autocmd FileType java                   nnoremap <buffer> <C-F> :%JavaFormat <CR>
    autocmd FileType java                   setl noexpandtab
augroup END

augroup filetype_cxx
    autocmd!
    autocmd FileType c,cpp                  setl tabstop=2 shiftwidth=2 softtabstop=2
augroup END

augroup filetype_misc
    autocmd!
    autocmd FileType vim                    setl iskeyword+=:,#
    autocmd FileType jade,pug               setl nolinebreak tw=0
    autocmd FileType css,scss               setl tabstop=2 shiftwidth=2
    autocmd FileType ruby                   setl tabstop=2 shiftwidth=2
    autocmd FileType yaml                   setl tabstop=4 shiftwidth=4
    autocmd FileType pig                    setl commentstring=--%s

    autocmd BufRead,BufNewFile *.gradle             set filetype=groovy
    autocmd BufRead,BufNewFile *.jade               set filetype=pug
    autocmd BufRead,BufNewFile /etc/nginx/sites-*/* set filetype=nginx
    autocmd BufRead,BufNewFile *.flex,*.jflex       set filetype=jflex
    autocmd BufRead,BufNewFile /etc/hosts*          set noexpandtab
augroup END

augroup filetype_help
    autocmd!
    autocmd FileType help                   nnoremap <buffer> q :helpclose <CR>
augroup END
" }}}

filetype plugin indent on

" vim: fdm=marker et fen fdl=0
