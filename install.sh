echo "Creating symlinks"

set -x

ln -fs $PWD/.tmux.conf ~/.tmux.conf
ln -fs $PWD/.dircolors ~/.dircolors
ln -fs $PWD/.eclimrc ~/.eclimrc
ln -fs $PWD/.zshrc ~/.zshrc
ln -fs $PWD/.config/nvim ~/.config/nvim
